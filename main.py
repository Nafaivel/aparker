import cairo
import gi
import threading
import time

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk, GObject


class Screen(Gtk.DrawingArea):
    """ This class is a Drawing Area"""
    def __init__(self):
        super(Screen,self).__init__()
        ## Connect to the "draw" signal
        self.connect("draw", self.on_draw)
        ## This is what gives the animation life!
        GObject.timeout_add(50, self.tick) # Go call tick every 50 whatsits.

    def tick(self):
        ## This invalidates the screen, causing the "draw" event to fire.
        rect = self.get_allocation()
        self.get_window().invalidate_rect(rect, True)
        return True # Causes timeout to tick again.

    ## When the "draw" event fires, this is run
    def on_draw(self, widget, event):
        self.cr = self.get_window().cairo_create()
        ## Call our draw function to do stuff.
        geom = self.get_window().get_geometry()
        self.draw(geom.width, geom.height)

class MyStuff(Screen):
    """This class is also a Drawing Area, coming from Screen."""
    def __init__(self):
        Screen.__init__(self)
        ## x,y is where I'm at
        self.x, self.y = 25, -25
        ## rx,ry is point of rotation
        self.rx, self.ry = -10, -25
        ## rot is angle counter
        self.rot = 0
        ## sx,sy is to mess with scale
        self.sx, self.sy = 1, 1

    def draw(self, width, height):
        ## A shortcut
        cr = self.cr

        ## First, let's shift 0,0 to be in the center of page
        ## This means:
        ##  -y | -y
        ##  -x | +x
        ## ----0------
        ##  -x | +x
        ##  +y | +y

        matrix = cairo.Matrix(1, 0, 0, 1, width/2, height/2)
        cr.transform(matrix) # Make it so...

        ## Now save that situation so that we can mess with it.
        ## This preserves the last context(the one at 0,0)
        ## and let's us do new stuff.
        cr.save()

        ## Now attempt to rotate something around a point
        ## Use a matrix to change the shape's position and rotation.

        ## First, make a matrix. Don't look at me, I only use this stuff :)
        ThingMatrix = cairo.Matrix(1, 0, 0, 1, 0, 0)

        ## Next, move the drawing to it's x,y
        cairo.Matrix.translate(ThingMatrix, self.x, self.y)
        cr.transform(ThingMatrix) # Changes the context to reflect that

        ## Now, change the matrix again to:
        cairo.Matrix.translate(ThingMatrix, self.rx, self.ry) # move it all to point of rotation
        cairo.Matrix.rotate(ThingMatrix, self.rot) # Do the rotation
        cairo.Matrix.translate(ThingMatrix, -self.rx, -self.ry) # move it back again
        cairo.Matrix.scale(ThingMatrix, self.sx, self.sy) # Now scale it all
        cr.transform(ThingMatrix) # and commit it to the context

        ## Now, whatever is draw is "under the influence" of the
        ## context and all that matrix magix we just did.
        self.drawCairoStuff(cr)

        ## Let's inc the angle a little
        self.rot += 0.1

        ## Now mess with scale too
        self.sx += 0 # Change to 0 to see if rotation is working...
        if self.sx > 4: self.sx=0.5
        self.sy = self.sx

        ## We restore to a clean context, to undo all that hocus-pocus
        cr.restore()

        ## Let's draw a crosshair so we can identify 0,0
        ## Drawn last to be above the red square.
        self.drawcross(cr)

    def drawCairoStuff(self, cr):
        ## Thrillingly, we draw a red rectangle.
        ## It's drawn such that 0,0 is in it's center.
        cr.rectangle(-25, -25, 50, 50)
        cr.set_source_rgb(1, 0, 0)
        cr.fill()
        ## Now a visual indicator of the point of rotation
        ## I have no idea(yet) how to keep this as a
        ## tiny dot when the entire thing scales.
        cr.set_source_rgb(1, 1, 1)
        cr.move_to(self.rx, self.ry)
        cr.line_to(self.rx+1, self.ry+1)
        cr.stroke()

    def drawcross(self, ctx):
        ## Also drawn around 0,0 in the center
        ctx.set_source_rgb(0, 0, 0)
        ctx.move_to(0,10)
        ctx.line_to(0, -10)
        ctx.move_to(-10, 0)
        ctx.line_to(10, 0)
        ctx.stroke()


class App():
        
    def __init__(self):
        uiFile = "ui.ui"
        builder = Gtk.Builder()
        builder.add_from_file(uiFile)

        widget = MyStuff()
        widget.show()

        da = builder.get_object("da")
        da.set_size_request(600, 600)
        da.connect("draw", self.mdraw)
        da.connect("draw", self.scribble_draw_event)
        da.connect('configure-event', self.scribble_configure_event)

        start_button = builder.get_object("start")
        start_button.connect("clicked", self.button_start_draw)

        stop_button = builder.get_object("stop")
        stop_button.connect("clicked", self.button_action)

        slower_button = builder.get_object("slower")
        slower_button .connect("clicked", self.button_action)

        faster_button = builder.get_object("faster")
        faster_button.connect("clicked", self.button_action)

        box = builder.get_object("Box")
        box.add(widget)

        window = builder.get_object("Main")
        # window = Gtk.Window()
        # window.set_size_request(500,500)
        window.connect("delete-event", Gtk.main_quit)
        window.show_all()

    def button_action(self, button):
        print("start")


    def button_start_draw(self, button):
        print("draw")


    def draw(self, da, cairo_ctx):
        check_size = 100
        spacing = 2

        xcount = 0
        i = spacing
        # width = da.get_allocated_width()
        width = 600
        height = da.get_allocated_height()

        

        while i < width:
            j = spacing
            ycount = xcount % 2  # start with even/odd depending on row
            while j < height:
                if ycount % 2:
                    # cairo_ctx.set_source_rgb(0.45777, 0, 0.45777)
                    cairo_ctx.set_source_rgb(0, 0, 0)
                else:
                    cairo_ctx.set_source_rgb(1, 1, 1)
                # If we're outside the clip this will do nothing.
                cairo_ctx.rectangle(i, j,
                                    check_size,
                                    check_size)
                cairo_ctx.fill()

                j += check_size + spacing
                ycount += 1
                threading.Thread(da.queue_draw())

            i += check_size + spacing
            xcount += 1
        return True

    def mdraw(self, da, cairo_ctx):
        width = 600
        height = da.get_allocated_height()
        carsize = [80,150]
        gap = 10

        cairo_ctx.set_source_rgb(1, 1, 1)
        cairo_ctx.rectangle(0, 0, width, height)
        cairo_ctx.fill()
        cairo_ctx.set_source_rgb(0, 0, 0)
        cairo_ctx.rectangle(width-carsize[0]-gap, 0+gap, carsize[0], carsize[1])
        cairo_ctx.fill()
        da.queue_draw()

        return True


    def scribble_draw_event(self, da, cairo_ctx):

        cairo_ctx.set_source_surface(self.surface, 0, 0)
        cairo_ctx.paint()

        return False

    
    def scribble_configure_event(self, da, event):

        allocation = da.get_allocation()
        self.surface = da.get_window().create_similar_surface(cairo.CONTENT_COLOR,
                                                              allocation.width,
                                                              allocation.height)

        cairo_ctx = cairo.Context(self.surface)
        cairo_ctx.set_source_rgb(1, 1, 1)
        cairo_ctx.paint()

        return True

def main(demoapp=None):
    App()
    Gtk.main()

if __name__ == '__main__':
    main()


